//
//  main.m
//  ZuoYeXingChen-iOS
//
//  Created by tao on 5/29/14.
//  Copyright (c) 2014 TaoZhou. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ZYAppDelegate.h"

int main(int argc, char * argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([ZYAppDelegate class]));
	}
}
