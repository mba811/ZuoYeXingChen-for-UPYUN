//
//  ZYXCPhotoCollectionViewHeader.h
//  ZuoYeXingChen-iOS
//
//  Created by tao on 5/27/14.
//  Copyright (c) 2014 TaoZhou. All rights reserved.
//


@interface ZYPhotoCollectionViewHeader : UICollectionReusableView

@property (nonatomic, strong, setter = setDate:) NSDate* date;

@end
